package com.example.ctsealarmproject;

public class AlarmModel {
    //Alarm attributes
    private int alarmId;
    private String alarmName;
    private String alarmTime;
    private int alarmTone;
    private String alarmState;

    public AlarmModel(){   }

    public AlarmModel(int alarmid,String alarmname,String alarmtime,int alarmtone,String alarmstate){
        this.alarmId = alarmid;
        this.alarmName = alarmname;
        this.alarmTime = alarmtime;
        this.alarmTone = alarmtone;
        this.alarmState = alarmstate;
    }


    public int getAlarmId() {
        return alarmId;
    }

    public void setAlarmId(int alarmId) {
        this.alarmId = alarmId;
    }

    public String getAlarmName() {
        return alarmName;
    }

    public void setAlarmName(String alarmName) {
        this.alarmName = alarmName;
    }

    public String getAlarmTime() {
        return alarmTime;
    }

    public void setAlarmTime(String alarmTime) {
        this.alarmTime = alarmTime;
    }

    public int getAlarmTone() {
        return alarmTone;
    }

    public void setAlarmTone(int alarmTone) {
        this.alarmTone = alarmTone;
    }

    public String getAlarmState() {
        return alarmState;
    }

    public void setAlarmState(String alarmState) {
        this.alarmState = alarmState;
    }
}
