package com.example.ctsealarmproject;

import android.app.AlertDialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    int alarmCount = 0;
    DatabaseHandler databaseHandler;

    //Alarm List
    List<AlarmModel> alarmModelList;

    TextView txtAlarmTime1;
    TextView txtAlarmTime2;
    TextView txtAlarmTime3;
    TextView txtAlarmTime4;
    TextView txtAlarmTime5;

    TextView txtAlarmName1;
    TextView txtAlarmName2;
    TextView txtAlarmName3;
    TextView txtAlarmName4;
    TextView txtAlarmName5;

    Button btnAlarmState1;
    Button btnAlarmState2;
    Button btnAlarmState3;
    Button btnAlarmState4;
    Button btnAlarmState5;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //Initialize the TextViews
        txtAlarmTime1 = (TextView) findViewById(R.id.txtAlarmTime1);
        txtAlarmTime2 = (TextView) findViewById(R.id.txtAlarmTime2);
        txtAlarmTime3 = (TextView) findViewById(R.id.txtAlarmTime3);
        txtAlarmTime4 = (TextView) findViewById(R.id.txtAlarmTime4);
        txtAlarmTime5 = (TextView) findViewById(R.id.txtAlarmTime5);

        txtAlarmName1 = (TextView) findViewById(R.id.txtAlarmName1);
        txtAlarmName2 = (TextView) findViewById(R.id.txtAlarmName2);
        txtAlarmName3 = (TextView) findViewById(R.id.txtAlarmName3);
        txtAlarmName4 = (TextView) findViewById(R.id.txtAlarmName4);
        txtAlarmName5 = (TextView) findViewById(R.id.txtAlarmName5);

        //Initialize the Buttons
        btnAlarmState1 = (Button) findViewById(R.id.btnAlarmState1);
        btnAlarmState2 = (Button) findViewById(R.id.btnAlarmState2);
        btnAlarmState3 = (Button) findViewById(R.id.btnAlarmState3);
        btnAlarmState4 = (Button) findViewById(R.id.btnAlarmState4);
        btnAlarmState5 = (Button) findViewById(R.id.btnAlarmState5);

        //Initialize the Database Connection
        databaseHandler = new DatabaseHandler(this);

        txtAlarmName1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if((1 <= alarmCount) && (alarmCount <= 5)){
                    Intent intentToSetAlarm = new Intent(MainActivity.this, SetAlarm.class);
                    intentToSetAlarm.putExtra("source","alarmNameClick MainActivity");
                    intentToSetAlarm.putExtra("alarmID",alarmModelList.get(0).getAlarmId());
                    startActivity(intentToSetAlarm);
                }

            }
        });

        txtAlarmName2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if((2 <= alarmCount) && (alarmCount <= 5)){
                    Intent intentToSetAlarm = new Intent(MainActivity.this, SetAlarm.class);
                    intentToSetAlarm.putExtra("source","alarmNameClick MainActivity");
                    intentToSetAlarm.putExtra("alarmID",alarmModelList.get(1).getAlarmId());
                    startActivity(intentToSetAlarm);
                }

            }
        });

        txtAlarmName3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if((3 <= alarmCount) && (alarmCount <= 5)){
                    Intent intentToSetAlarm = new Intent(MainActivity.this, SetAlarm.class);
                    intentToSetAlarm.putExtra("source","alarmNameClick MainActivity");
                    intentToSetAlarm.putExtra("alarmID",alarmModelList.get(2).getAlarmId());
                    startActivity(intentToSetAlarm);
                }

            }
        });

        txtAlarmName4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if((4 <= alarmCount) && (alarmCount <= 5)){
                    Intent intentToSetAlarm = new Intent(MainActivity.this, SetAlarm.class);
                    intentToSetAlarm.putExtra("source","alarmNameClick MainActivity");
                    intentToSetAlarm.putExtra("alarmID",alarmModelList.get(3).getAlarmId());
                    startActivity(intentToSetAlarm);
                }

            }
        });

        txtAlarmName5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(5 <= alarmCount) {
                    Intent intentToSetAlarm = new Intent(MainActivity.this, SetAlarm.class);
                    intentToSetAlarm.putExtra("source","alarmNameClick MainActivity");
                    intentToSetAlarm.putExtra("alarmID",alarmModelList.get(4).getAlarmId());
                    startActivity(intentToSetAlarm);
                }

            }
        });

        //Set to turn on/off alarms

        btnAlarmState1.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                String btnValue = btnAlarmState1.getText().toString();
                if(btnValue.equalsIgnoreCase("on")){
                    btnValue = "off";
                }
                else if(btnValue.equalsIgnoreCase("off")){
                    btnValue = "on";
                }
                else {
                    return;
                }
                btnAlarmState1.setText(btnValue);
                if((1 <= alarmCount) && (alarmCount <= 5)) {
                    Intent intentToSetAlarm = new Intent(MainActivity.this, SetAlarm.class);
                    intentToSetAlarm.putExtra("source","onOffButtonClick MainActivity");
                    intentToSetAlarm.putExtra("alarmID",alarmModelList.get(0).getAlarmId());
                    intentToSetAlarm.putExtra("alarmState",btnValue);
                    startActivity(intentToSetAlarm);
                }

            }
        });

        btnAlarmState2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String btnValue = btnAlarmState2.getText().toString();
                if(btnValue.equalsIgnoreCase("on")){
                    btnValue = "off";
                }
                else if(btnValue.equalsIgnoreCase("off")){
                    btnValue = "on";
                }
                else {
                    return;
                }
                btnAlarmState2.setText(btnValue);

                if((2 <= alarmCount) && (alarmCount <= 5)){
                    Intent intentToSetAlarm = new Intent(MainActivity.this, SetAlarm.class);
                    intentToSetAlarm.putExtra("source","onOffButtonClick MainActivity");
                    intentToSetAlarm.putExtra("alarmID",alarmModelList.get(1).getAlarmId());
                    intentToSetAlarm.putExtra("alarmState",btnValue);
                    startActivity(intentToSetAlarm);
                }

            }
        });

        btnAlarmState3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String btnValue = btnAlarmState3.getText().toString();
                if(btnValue.equalsIgnoreCase("on")){
                    btnValue = "off";
                }
                else if(btnValue.equalsIgnoreCase("off")){
                    btnValue = "on";
                }
                else {
                    return;
                }
                btnAlarmState3.setText(btnValue);

                if((3 <= alarmCount) && (alarmCount <= 5)){
                    Intent intentToSetAlarm = new Intent(MainActivity.this, SetAlarm.class);
                    intentToSetAlarm.putExtra("source","onOffButtonClick MainActivity");
                    intentToSetAlarm.putExtra("alarmID",alarmModelList.get(2).getAlarmId());
                    intentToSetAlarm.putExtra("alarmState",btnValue);
                    startActivity(intentToSetAlarm);
                }

            }
        });

        btnAlarmState4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String btnValue = btnAlarmState4.getText().toString();
                if(btnValue.equalsIgnoreCase("on")){
                    btnValue = "off";
                }
                else if(btnValue.equalsIgnoreCase("off")){
                    btnValue = "on";
                }
                else {
                    return;
                }
                btnAlarmState4.setText(btnValue);

                if((4 <= alarmCount) && (alarmCount <= 5)){
                    Intent intentToSetAlarm = new Intent(MainActivity.this, SetAlarm.class);
                    intentToSetAlarm.putExtra("source","onOffButtonClick MainActivity");
                    intentToSetAlarm.putExtra("alarmID",alarmModelList.get(3).getAlarmId());
                    intentToSetAlarm.putExtra("alarmState",btnValue);
                    startActivity(intentToSetAlarm);
                }

            }
        });

        btnAlarmState5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String btnValue = btnAlarmState5.getText().toString();
                if(btnValue.equalsIgnoreCase("on")){
                    btnValue = "off";
                }
                else if(btnValue.equalsIgnoreCase("off")){
                    btnValue = "on";
                }
                else {
                    return;
                }
                btnAlarmState5.setText(btnValue);

                if(5 <= alarmCount) {
                    Intent intentToSetAlarm = new Intent(MainActivity.this, SetAlarm.class);
                    intentToSetAlarm.putExtra("source","onOffButtonClick MainActivity");
                    intentToSetAlarm.putExtra("alarmID",alarmModelList.get(4).getAlarmId());
                    intentToSetAlarm.putExtra("alarmState",btnValue);
                    startActivity(intentToSetAlarm);
                }

            }
        });



    }

    @Override
    protected void onStart() {
        super.onStart();

        getAllAlarmData();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId()== R.id.add) {
            //add the function to perform here
            Intent intentToSetAlarm = new Intent(this, SetAlarm.class);
            intentToSetAlarm.putExtra("source","addAlarmClick MainActivity");
            this.startActivity(intentToSetAlarm);
            Log.e("Add new alarm button","pressed");
            return(true);
        }

        return(super.onOptionsItemSelected(item));
    }


    public void getAllAlarmData(){

         alarmCount = databaseHandler.getAlarmCount();
         if(alarmCount == 0){
             Log.e("Alarm count: ","zero");

             txtAlarmName1.setText("");
             txtAlarmName2.setText("");
             txtAlarmName3.setText("");
             txtAlarmName4.setText("");
             txtAlarmName5.setText("");

             txtAlarmTime1.setText("");
             txtAlarmTime2.setText("");
             txtAlarmTime3.setText("");
             txtAlarmTime4.setText("");
             txtAlarmTime5.setText("");

             btnAlarmState1.setText("");
             btnAlarmState2.setText("");
             btnAlarmState3.setText("");
             btnAlarmState4.setText("");
             btnAlarmState5.setText("");

             btnAlarmState1.setVisibility(View.INVISIBLE);
             btnAlarmState2.setVisibility(View.INVISIBLE);
             btnAlarmState3.setVisibility(View.INVISIBLE);
             btnAlarmState4.setVisibility(View.INVISIBLE);
             btnAlarmState5.setVisibility(View.INVISIBLE);
         }
         else {
             txtAlarmName1.setText("");
             txtAlarmName2.setText("");
             txtAlarmName3.setText("");
             txtAlarmName4.setText("");
             txtAlarmName5.setText("");

             txtAlarmTime1.setText("");
             txtAlarmTime2.setText("");
             txtAlarmTime3.setText("");
             txtAlarmTime4.setText("");
             txtAlarmTime5.setText("");

             btnAlarmState1.setText("");
             btnAlarmState2.setText("");
             btnAlarmState3.setText("");
             btnAlarmState4.setText("");
             btnAlarmState5.setText("");
             Log.e("Alarm count: ",String.valueOf(alarmCount));

             alarmModelList = databaseHandler.getAllAlarmData();

             Log.e("alarm Data 1",alarmModelList.get(0).getAlarmName());
             Log.e("alarm Data 2",alarmModelList.get(0).getAlarmTime());
             Log.e("alarm Data 3",alarmModelList.get(0).getAlarmState());

             if(alarmCount == 5){
                 txtAlarmName1.setText(alarmModelList.get(0).getAlarmName());
                 txtAlarmName2.setText(alarmModelList.get(1).getAlarmName());
                 txtAlarmName3.setText(alarmModelList.get(2).getAlarmName());
                 txtAlarmName4.setText(alarmModelList.get(3).getAlarmName());
                 txtAlarmName5.setText(alarmModelList.get(4).getAlarmName());


                 txtAlarmTime1.setText(getAdjustedAlarmTime(0));
                 txtAlarmTime2.setText(getAdjustedAlarmTime(1));
                 txtAlarmTime3.setText(getAdjustedAlarmTime(2));
                 txtAlarmTime4.setText(getAdjustedAlarmTime(3));
                 txtAlarmTime5.setText(getAdjustedAlarmTime(4));

                 btnAlarmState1.setText(alarmModelList.get(0).getAlarmState());
                 btnAlarmState2.setText(alarmModelList.get(1).getAlarmState());
                 btnAlarmState3.setText(alarmModelList.get(2).getAlarmState());
                 btnAlarmState4.setText(alarmModelList.get(3).getAlarmState());
                 btnAlarmState5.setText(alarmModelList.get(4).getAlarmState());

                 btnAlarmState1.setVisibility(View.VISIBLE);
                 btnAlarmState2.setVisibility(View.VISIBLE);
                 btnAlarmState3.setVisibility(View.VISIBLE);
                 btnAlarmState4.setVisibility(View.VISIBLE);
                 btnAlarmState5.setVisibility(View.VISIBLE);

             }
             else if(alarmCount == 4){
                 txtAlarmName1.setText(alarmModelList.get(0).getAlarmName());
                 txtAlarmName2.setText(alarmModelList.get(1).getAlarmName());
                 txtAlarmName3.setText(alarmModelList.get(2).getAlarmName());
                 txtAlarmName4.setText(alarmModelList.get(3).getAlarmName());

                 txtAlarmTime1.setText(getAdjustedAlarmTime(0));
                 txtAlarmTime2.setText(getAdjustedAlarmTime(1));
                 txtAlarmTime3.setText(getAdjustedAlarmTime(2));
                 txtAlarmTime4.setText(getAdjustedAlarmTime(3));

                 btnAlarmState1.setText(alarmModelList.get(0).getAlarmState());
                 btnAlarmState2.setText(alarmModelList.get(1).getAlarmState());
                 btnAlarmState3.setText(alarmModelList.get(2).getAlarmState());
                 btnAlarmState4.setText(alarmModelList.get(3).getAlarmState());

                 btnAlarmState1.setVisibility(View.VISIBLE);
                 btnAlarmState2.setVisibility(View.VISIBLE);
                 btnAlarmState3.setVisibility(View.VISIBLE);
                 btnAlarmState4.setVisibility(View.VISIBLE);
                 btnAlarmState5.setVisibility(View.INVISIBLE);
             }
             else if(alarmCount == 3){
                 txtAlarmName1.setText(alarmModelList.get(0).getAlarmName());
                 txtAlarmName2.setText(alarmModelList.get(1).getAlarmName());
                 txtAlarmName3.setText(alarmModelList.get(2).getAlarmName());

                 txtAlarmTime1.setText(getAdjustedAlarmTime(0));
                 txtAlarmTime2.setText(getAdjustedAlarmTime(1));
                 txtAlarmTime3.setText(getAdjustedAlarmTime(2));

                 btnAlarmState1.setText(alarmModelList.get(0).getAlarmState());
                 btnAlarmState2.setText(alarmModelList.get(1).getAlarmState());
                 btnAlarmState3.setText(alarmModelList.get(2).getAlarmState());

                 btnAlarmState1.setVisibility(View.VISIBLE);
                 btnAlarmState2.setVisibility(View.VISIBLE);
                 btnAlarmState3.setVisibility(View.VISIBLE);
                 btnAlarmState4.setVisibility(View.INVISIBLE);
                 btnAlarmState5.setVisibility(View.INVISIBLE);
             }
             else if(alarmCount == 2){
                 txtAlarmName1.setText(alarmModelList.get(0).getAlarmName());
                 txtAlarmName2.setText(alarmModelList.get(1).getAlarmName());

                 txtAlarmTime1.setText(getAdjustedAlarmTime(0));
                 txtAlarmTime2.setText(getAdjustedAlarmTime(1));

                 btnAlarmState1.setText(alarmModelList.get(0).getAlarmState());
                 btnAlarmState2.setText(alarmModelList.get(1).getAlarmState());

                 btnAlarmState1.setVisibility(View.VISIBLE);
                 btnAlarmState2.setVisibility(View.VISIBLE);
                 btnAlarmState3.setVisibility(View.INVISIBLE);
                 btnAlarmState4.setVisibility(View.INVISIBLE);
                 btnAlarmState5.setVisibility(View.INVISIBLE);
             }
             else if(alarmCount == 1){
                 txtAlarmName1.setText(alarmModelList.get(0).getAlarmName());

                 txtAlarmTime1.setText(getAdjustedAlarmTime(0));
//                 txtAlarmTime1.setText(alarmModelList.get(0).getAlarmTime());

                 btnAlarmState1.setText(alarmModelList.get(0).getAlarmState());

                 btnAlarmState1.setVisibility(View.VISIBLE);
                 btnAlarmState2.setVisibility(View.INVISIBLE);
                 btnAlarmState3.setVisibility(View.INVISIBLE);
                 btnAlarmState4.setVisibility(View.INVISIBLE);
                 btnAlarmState5.setVisibility(View.INVISIBLE);

             }
             else {
                 showMessage("Error","Nothing found");
             }


         }


    }

    public String getAdjustedAlarmTime(int num){
        String alarmTime = alarmModelList.get(num).getAlarmTime();

        String[] stringTimeArray = alarmTime.split(":");
        int alarmMinute = Integer.parseInt(stringTimeArray[1]);
        if(alarmMinute < 10){
            String alarmMinuteString = "0" + String.valueOf(alarmMinute);
            stringTimeArray[1] = alarmMinuteString;
            alarmTime = stringTimeArray[0]+":"+stringTimeArray[1];
            return alarmTime;
        }
        return alarmTime;
    }

    public void showMessage(String title,String Message){
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setCancelable(true);
        builder.setTitle(title);
        builder.setMessage(Message);
        builder.show();
    }



}
