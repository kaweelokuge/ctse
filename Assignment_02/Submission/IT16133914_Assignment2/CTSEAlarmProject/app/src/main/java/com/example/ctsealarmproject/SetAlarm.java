package com.example.ctsealarmproject;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.media.MediaPlayer;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TimePicker;

import java.util.Calendar;

public class SetAlarm extends AppCompatActivity implements AdapterView.OnItemSelectedListener {

    TimePicker tpTimePicker;
    EditText txtAlarmName;
    Button btnSaveAlarm;
    Button btnCancelAlarm;
    Button btnRemoveAlarm;

    Spinner spinner;

    Context context;
    AlarmManager alarmManager;
    final Calendar calendar = Calendar.getInstance();

    String alarmNameString;
    String alarmTime;
    int chooseRingtoneSound;
    String alarmState = "off";

    DatabaseHandler databaseHandler;
    MediaPlayer mediaPlayerSong;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_set_alarm);

        this.context = this;

        //Initialize the Database Connection
        databaseHandler = new DatabaseHandler(this);

        //Initialize alarm manager
        alarmManager = (AlarmManager) getSystemService(ALARM_SERVICE);

        //Initialize time picker
        tpTimePicker = (TimePicker) findViewById(R.id.timePicker);
        tpTimePicker.setIs24HourView(true);

        //Initialize edit text
        txtAlarmName = (EditText) findViewById(R.id.pltxtAlarmName);

        //Initialize Buttons
        btnSaveAlarm = findViewById(R.id.btnSave);
        btnCancelAlarm = findViewById(R.id.btnCancel);
        btnRemoveAlarm = findViewById(R.id.btnRemove);

        //create the spinner in the SetAlarm UI

        spinner = (Spinner) findViewById(R.id.spinnerRingtone);

        //Create an ArrayAdapter using the string array and a default spinner layout
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this,R.array.alarm_array, android.R.layout.simple_spinner_item);

        //Specify the layout to use when the list of choices appears
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        //Apply the adapter to the spinner
        spinner.setAdapter(adapter);

        //Set an onClick listener to the onItemSelected method
        spinner.setOnItemSelectedListener(this);

    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        //An item was selected. You can retrieve the selected item using
        //parent.getItemAtPosition(pos)

        //Outputting the  id user has selected
        chooseRingtoneSound = (int) id;

        if(chooseRingtoneSound == 0){
            //Create an instance of media player
            mediaPlayerSong = MediaPlayer.create(this,R.raw.beautiful_christmas_tune);
            if(mediaPlayerSong.isPlaying()){
                mediaPlayerSong.stop();
            }
        }
        else if(chooseRingtoneSound == 1){
//            if(mediaPlayerSong.isPlaying()){
//                mediaPlayerSong.stop();
//            }
            //Create an instance of media player
            mediaPlayerSong = MediaPlayer.create(this,R.raw.beautiful_christmas_tune);
            //Start the ringtone
            mediaPlayerSong.start();
        }
        else if(chooseRingtoneSound == 2){
//            if(mediaPlayerSong.isPlaying()){
//                mediaPlayerSong.stop();
//            }
            mediaPlayerSong = MediaPlayer.create(this,R.raw.dreamy_christmas_bells);
            mediaPlayerSong.start();
        }
        else if(chooseRingtoneSound == 3){
//            if(mediaPlayerSong.isPlaying()){
//                mediaPlayerSong.stop();
//            }
            mediaPlayerSong = MediaPlayer.create(this,R.raw.goodmorning);
            mediaPlayerSong.start();
        }
        else if(chooseRingtoneSound == 4){
//            if(mediaPlayerSong.isPlaying()){
//                mediaPlayerSong.stop();
//            }
            mediaPlayerSong = MediaPlayer.create(this,R.raw.soft_bells);
            mediaPlayerSong.start();
        }
        else if(chooseRingtoneSound == 5){
//            if(mediaPlayerSong.isPlaying()){
//                mediaPlayerSong.stop();
//            }
            mediaPlayerSong = MediaPlayer.create(this,R.raw.wake_up_will_you);
            mediaPlayerSong.start();
        }
        else {
            if(mediaPlayerSong.isPlaying()){
                mediaPlayerSong.stop();
            }
        }
    }


    @Override
    public void onNothingSelected(AdapterView<?> parent) { }


    @Override
    protected void onStart() {
        super.onStart();

        //Hide the Remove Button
        btnRemoveAlarm.setVisibility(View.GONE);

        final Intent intent = getIntent();
        String source = intent.getExtras().getString("source");

        if(source.equals("addAlarmClick MainActivity")){

            //onClick Listener to save the alarm
            btnSaveAlarm.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    //Set the alarm

                    if(mediaPlayerSong.isPlaying())
                        mediaPlayerSong.stop();

                    //Assigning the values to the calendar instance that we picked in the time picker
                    calendar.set(Calendar.HOUR_OF_DAY, tpTimePicker.getHour());
                    calendar.set(Calendar.MINUTE, tpTimePicker.getMinute());

                    //get the int values of hour and minute
                    int hour = tpTimePicker.getHour();
                    int minute = tpTimePicker.getMinute();

                    //Convert the int values into Strings
                    String hourString = String.valueOf(hour);
                    String minuteString = String.valueOf(minute);

                    if(minute<10){
                        //10:8-->10:08
                        minuteString = "0"+minuteString;
                    }

                    Log.e("Alarm set to: ",hourString+":"+minuteString);

                    //Put in extra string into intentToAlarmReceiver
                    //Tells the clock that you pressed the alarm on button
//                intentToAlarmReceiver.putExtra("extra","alarm on");

                    //Put an extra int value into intentToAlarmReceiver
                    //Tells the clock that you want a certain value from the drop down menu/spinner
//                intentToAlarmReceiver.putExtra("ringtoneChoice",chooseRingtoneSound);


                    //Setting a specific id to identify the alarm
                    int _id = (int) System.currentTimeMillis();

                    if((txtAlarmName.getText().toString().equals("")) || (txtAlarmName.getText().toString().equals("Alarm Name")) ){
                        alarmNameString = "Alarm";
                    }
                    else {
                        alarmNameString = txtAlarmName.getText().toString();
                    }


                    //Insert the alarm to the DB

                    alarmTime = hour+":"+minute;
                    alarmState = "on";

                    boolean alarmAdded = databaseHandler.insertAlarmData(new AlarmModel(_id,alarmNameString,alarmTime,chooseRingtoneSound,alarmState));

                    if(alarmAdded){
                        Log.e("Alarm insert: ","Success");
                    }
                    else {
                        Log.e("Alarm insert: ","Failed");
                    }

                    alarmState = "off";

                    setAlarm(_id, chooseRingtoneSound, hour, minute);

                    startActivity(new Intent(SetAlarm.this,MainActivity.class));

                }
            });


        }


        if(source.equals("alarmNameClick MainActivity")){

            //Display the Remove Button
            btnRemoveAlarm.setVisibility(View.VISIBLE);

            final int alarmID = intent.getExtras().getInt("alarmID");
            final AlarmModel alarmModel = databaseHandler.getAlarmData(alarmID);

            alarmTime = alarmModel.getAlarmTime();
            String[] stringTimeArray = alarmTime.split(":");

            String alarmHour = stringTimeArray[0];
            String alarmMinute = stringTimeArray[1];

            alarmNameString = alarmModel.getAlarmName();
            chooseRingtoneSound = alarmModel.getAlarmTone();

            //Set the saved Hour and Minute
            tpTimePicker.setHour(Integer.parseInt(alarmHour));
            tpTimePicker.setMinute(Integer.parseInt(alarmMinute));

            //Set the saved alarm name
            txtAlarmName.setText(alarmNameString);
            spinner.setSelection(chooseRingtoneSound);

            //Update the Alarm
            btnSaveAlarm.setText("Update");
            btnSaveAlarm.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(mediaPlayerSong.isPlaying())
                        mediaPlayerSong.stop();

                    alarmModel.setAlarmName(txtAlarmName.getText().toString());
                    //get the int values of hour and minute
                    int hour = tpTimePicker.getHour();
                    int minute = tpTimePicker.getMinute();

                    alarmTime = hour+":"+minute;
                    alarmModel.setAlarmTime(alarmTime);
                    alarmModel.setAlarmTone(chooseRingtoneSound);
                    alarmModel.setAlarmState("off");

                    updateAlarm(alarmModel);
                }
            });

            //Remove the alarm
            btnRemoveAlarm.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(mediaPlayerSong.isPlaying())
                        mediaPlayerSong.stop();
                    removeAlarm(alarmID);
                }
            });




        }

        if(source.equals("onOffButtonClick MainActivity")){

            final int alarmID = intent.getExtras().getInt("alarmID");
            alarmState = intent.getExtras().getString("alarmState");
            AlarmModel alarmModel = databaseHandler.getAlarmData(alarmID);

            alarmTime = alarmModel.getAlarmTime();
            String[] stringTimeArray = alarmTime.split(":");

            String alarmHour = stringTimeArray[0];
            String alarmMinute = stringTimeArray[1];

            chooseRingtoneSound = alarmModel.getAlarmTone();

            alarmModel.setAlarmState(alarmState);

            databaseHandler.updateAlarmData(alarmModel);
            if(alarmState.equalsIgnoreCase("on")){
                setAlarm(alarmID,chooseRingtoneSound,Integer.parseInt(alarmHour),Integer.parseInt(alarmMinute));
            }
            else if(alarmState.equalsIgnoreCase("off")) {
                unSetAlarm(alarmID);
            }
            else {
                Log.e("Alarm State: ",alarmState);
            }

            Intent intentToMainActivity = new Intent(SetAlarm.this, MainActivity.class);

            intentToMainActivity.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            intentToMainActivity.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);

            startActivity(intentToMainActivity);
        }

        if(source.equals("QuestionActivity")){

            final int alarmID = intent.getExtras().getInt("alarmID");
            AlarmModel alarmModel = databaseHandler.getAlarmData(alarmID);

            alarmTime = alarmModel.getAlarmTime();
            String[] stringTimeArray = alarmTime.split(":");

            String alarmHour = stringTimeArray[0];
            String alarmMinute = stringTimeArray[1];

            chooseRingtoneSound = alarmModel.getAlarmTone();

            alarmModel.setAlarmState("off");
            databaseHandler.updateAlarmData(alarmModel);

            unSetAlarm(alarmID);

            Intent intentToMainActivity = new Intent(SetAlarm.this, MainActivity.class);

            intentToMainActivity.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            intentToMainActivity.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);

            startActivity(intentToMainActivity);
        }

        btnCancelAlarm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(mediaPlayerSong.isPlaying())
                    mediaPlayerSong.stop();

                startActivity(new Intent(SetAlarm.this,MainActivity.class));
            }

        });

    }

    //Modified
    private void setAlarm(int alarmId, int alarmSound, int hour, int minute) {
        Intent intentToAlarmReceiver = new Intent(this.context,AlarmReceiver.class);
        intentToAlarmReceiver.putExtra("alarmID",alarmId);
        intentToAlarmReceiver.putExtra("extra","alarm on");
        intentToAlarmReceiver.putExtra("ringtoneChoice",alarmSound);
        intentToAlarmReceiver.putExtra("source", "SetAlarm");



//        Log.e("alarm info",txtAlarmName+" "+String.valueOf(alarmId)+" "+String.valueOf(alarmSound));
        PendingIntent pendingIntent = PendingIntent.getBroadcast(SetAlarm.this,alarmId,intentToAlarmReceiver,PendingIntent.FLAG_NO_CREATE);

        if (pendingIntent != null) {
            Log.d("alarm", "existing");
            alarmManager.cancel(pendingIntent);
        }
        pendingIntent = PendingIntent.getBroadcast(SetAlarm.this, alarmId, intentToAlarmReceiver, PendingIntent.FLAG_UPDATE_CURRENT);

        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.MILLISECOND, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MINUTE, minute);
        calendar.set(Calendar.HOUR_OF_DAY, hour);

        alarmManager.setRepeating(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(), AlarmManager.INTERVAL_DAY, pendingIntent);
//        alarmManager.set(AlarmManager.RTC_WAKEUP,calendar.getTimeInMillis(),pendingIntent);
    }


    private void unSetAlarm(int alarmId){
        //This method turns off the alarm

        //Unset/Cancel Alarm Note- Does not stop the ringtone
        Intent intentToAlarmReceiver = new Intent(this.context,AlarmReceiver.class);
        alarmManager.cancel(PendingIntent.getBroadcast(context, alarmId, intentToAlarmReceiver, PendingIntent.FLAG_UPDATE_CURRENT));

        AlarmModel alarmModel = databaseHandler.getAlarmData(alarmId);

        //Make alarm state to off in database
        alarmModel.setAlarmState("off");

        //Put in extra string into intentToAlarmReceiver
        //Tells the clock that you pressed the alarm on button
        intentToAlarmReceiver.putExtra("source","UnSetAlarm");
        intentToAlarmReceiver.putExtra("extra","alarm off");
        intentToAlarmReceiver.putExtra("alarmID",alarmId);
        intentToAlarmReceiver.putExtra("ringtoneChoice",alarmModel.getAlarmTone());

        //Put in extra int value to the alarm off to prevent Null Pointer Exception
        intentToAlarmReceiver.putExtra("ringtoneChoice",chooseRingtoneSound);

        //Stop the ringtone
        sendBroadcast(intentToAlarmReceiver);

    }

    private void removeAlarm(int alarmId){
        //This method deletes the alarm
        unSetAlarm(alarmId);
        databaseHandler.deleteDataAlarmTable(databaseHandler.getAlarmData(alarmId));

        startActivity(new Intent(SetAlarm.this,MainActivity.class));
    }

    private void updateAlarm(AlarmModel alarmModel){
        unSetAlarm(alarmModel.getAlarmId());
        databaseHandler.updateAlarmData(alarmModel);
        startActivity(new Intent(SetAlarm.this,MainActivity.class));
    }

}

