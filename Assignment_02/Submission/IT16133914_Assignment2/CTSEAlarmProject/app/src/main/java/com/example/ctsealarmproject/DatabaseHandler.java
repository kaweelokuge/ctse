package com.example.ctsealarmproject;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

public class DatabaseHandler extends SQLiteOpenHelper {

    private static final int DATABASE_VERSION = 1;
    private static final String DATABASE_NAME = "Alarm.db";
    private static final String FIRST_TABLE_NAME = "alarm_table";
    private static final String COL_1 = "ALARMID";
    private static final String COL_2 = "ALARMNAME";
    private static final String COL_3 = "ALARMTIME";
    private static final String COL_4 = "ALARMTONE";
    private static final String COL_5 = "ALARMSTATE";

    public DatabaseHandler(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("CREATE TABLE " + FIRST_TABLE_NAME + " (ALARMID INTEGER PRIMARY KEY,ALARMNAME TEXT,ALARMTIME TEXT,ALARMTONE INTEGER,ALARMSTATE TEXT)");
        /*String CREATE_ALARM_TABLE = "CREATE TABLE " + FIRST_TABLE_NAME + "("
                + COL_1 + " INTEGER PRIMARY KEY," + KEY_NAME + " TEXT,"
                + KEY_PH_NO + " TEXT" + ")";
        db.execSQL(CREATE_ALARM_TABLE);*/
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + FIRST_TABLE_NAME);
        onCreate(db);
    }

    //Insert an Alarm
    public boolean insertAlarmData(AlarmModel alarmModel) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();

        //Logging alarm info
        Log.e("COL_1", String.valueOf(alarmModel.getAlarmId()));
        Log.e("COL_2", String.valueOf(alarmModel.getAlarmName()));
        Log.e("COL_3", String.valueOf(alarmModel.getAlarmTime()));
        Log.e("COL_4", String.valueOf(alarmModel.getAlarmTone()));
        Log.e("COL_5", String.valueOf(alarmModel.getAlarmState()));

        contentValues.put(COL_1, alarmModel.getAlarmId());
        contentValues.put(COL_2, alarmModel.getAlarmName());
        contentValues.put(COL_3, alarmModel.getAlarmTime());
        contentValues.put(COL_4, alarmModel.getAlarmTone());
        contentValues.put(COL_5, alarmModel.getAlarmState());

        long result = db.insert(FIRST_TABLE_NAME, null, contentValues);
        db.close();
        return result != -1;

    }

    //Get a single alarm
    public AlarmModel getAlarmData(int alarmId){
        SQLiteDatabase db = this.getReadableDatabase();

        Cursor cursor = db.query(FIRST_TABLE_NAME, new String[] { COL_1, COL_2, COL_3, COL_4, COL_5 }, COL_1 + "=?", new String[] { String.valueOf(alarmId) }, null, null, null, null);
        if (cursor != null)
            cursor.moveToFirst();

        AlarmModel alarmModel = new AlarmModel(Integer.parseInt(cursor.getString(0)),cursor.getString(1), cursor.getString(2),Integer.parseInt(cursor.getString(3)),cursor.getString(4));
        // return contact
        return alarmModel;

    }

    public List<AlarmModel> getAllAlarmData() {

        List<AlarmModel> alarmList = new ArrayList<AlarmModel>();

        // Select All Query
        String selectQuery = "SELECT  * FROM " + FIRST_TABLE_NAME;

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                AlarmModel alarmModel = new AlarmModel();
                alarmModel.setAlarmId(Integer.parseInt(cursor.getString(0)));
                alarmModel.setAlarmName(cursor.getString(1));
                alarmModel.setAlarmTime(cursor.getString(2));
                alarmModel.setAlarmTone(Integer.parseInt(cursor.getString(3)));
                alarmModel.setAlarmState(cursor.getString(4));

                // Adding contact to list
                alarmList.add(alarmModel);
            } while (cursor.moveToNext());
        }

        // return contact list
        return alarmList;

    }

    public int updateAlarmData(AlarmModel alarmModel){
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();

        contentValues.put(COL_2, alarmModel.getAlarmName());
        contentValues.put(COL_3, alarmModel.getAlarmTime());
        contentValues.put(COL_4, alarmModel.getAlarmTone());
        contentValues.put(COL_5, alarmModel.getAlarmState());

        return db.update(FIRST_TABLE_NAME, contentValues, "ALARMID = ?", new String[]{String.valueOf(alarmModel.getAlarmId())});
    }

    public int deleteDataAlarmTable(AlarmModel alarmModel) {
        SQLiteDatabase db = this.getWritableDatabase();
        int deletedRows = db.delete(FIRST_TABLE_NAME, "ALARMID = ?", new String[]{String.valueOf(alarmModel.getAlarmId())});
        db.close();

        return deletedRows;
    }


    public int getAlarmCount () {
        String countQuery = "SELECT  * FROM " + FIRST_TABLE_NAME;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(countQuery, null);
        int count = cursor.getCount();

        cursor.close();
        return count;
    }

}



